import java.util.*;
public class Problem4 {
    public static void main(String[] args) {
        int a,b,c,d;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter four Integer");
        a = sc.nextInt();
        b = sc.nextInt();
        c = sc.nextInt();
        d = sc.nextInt();
        
        Calulator(a,b);
        Calulator(a,b,c);
        Calulator(a,b,c,d);

    }
    public static void Calulator(int a, int b){
        System.out.printf("Addition: %d , Multiplication: %d \n",a+b,a*b );
    }
    public static void Calulator(int a, int b,int c){
        System.out.printf("Addition: %d , Multiplication: %d \n",a+b+c,a*b*c );
    }
    public static void Calulator(int a, int b, int c, int d){
        System.out.printf("Addition: %d , Multiplication: %d \n" ,a+b+c+d,a*b*c*d );
    }
}
