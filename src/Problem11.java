import java.util.*;
public class Problem11 {
    public static void main(String[] args) {
     String sentence = new String();
     String loweredSentence = new String();
     char[] charArray = new char[26];
     boolean flag = false;
     System.out.println("Enter a string to check pangram");
     Scanner sc = new Scanner(System.in);
     sentence = sc.nextLine();
     if(sentence.length()>= 26) {
         loweredSentence = sentence.toLowerCase();
         for (int i = 0; i < loweredSentence.length(); i++) {
             if (loweredSentence.charAt(i) >= 'a' && loweredSentence.charAt(i) <= 'z') {
                int index = loweredSentence.charAt(i) - 'a';
                charArray[index] = loweredSentence.charAt(i);
             }
         }
     }
     String cs = new String(charArray);
     if(cs.equals("abcdefghijklmnopqrstuvwxyz")){
         System.out.println("Pangram");
     }
     else {
         System.out.println("Not a Pangram");
     }
    }
}
