import java.util.*;
class Student  implements Comparable<Student>{
    int rollNo,age;
    String name;
    public Student(int rollNo, String name, int age){
        this.rollNo = rollNo;
        this.name = name;
        this.age = age;
    }
    public int compareTo(Student person) {
        return Integer.compare(this.rollNo, person.rollNo);

    }
}
public class Problem8 {
    public static void main(String[] args) {
        boolean flag = true;
        int rollNo,age;
        String name, choice;
        ArrayList<Student> studentList  = new ArrayList<Student>();
        Scanner sc = new Scanner(System.in);
        while(flag) {
            System.out.println("Enter Student roll no. , Name and age");
            rollNo = sc.nextInt();
            name = sc.next();
            age = sc.nextInt();
            Student s1 = new Student(rollNo, name, age);
            studentList.add(s1);
            System.out.println("Did you want to add more record press YES");
            Scanner scan = new Scanner(System.in);
            choice = scan.nextLine().toUpperCase();
            if(!choice.equals("YES")){
                flag = false;
            }
        }

        Collections.sort(studentList);

        Iterator itr = studentList.iterator();
        while(itr.hasNext()){
            Student st = (Student)itr.next();
            System.out.println(st.rollNo+" "+st.name+" "+st.age);
        }
        }

}