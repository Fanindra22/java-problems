import java.util.ArrayList;
import java.util.Iterator;
import java.util.Scanner;

public class Problem10 {
    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }
    public static void main(String[] args) {
        String choice;
        int data;
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        System.out.println("Enter the all the elements of Array List (Enter Alphabet to break)");
        while(true) {
            Scanner sc = new Scanner(System.in);
            choice = sc.next();
            if (!isNumeric(choice)) {
                break;
            }
            data = Integer.parseInt(choice);
            arrayList.add(data);
        }
        System.out.println("ArrayList Before Sorting");
        System.out.println(arrayList);

        for(int i = 0; i < arrayList.size(); i++) {
            for (int j = i + 1; j < arrayList.size(); j++) {
                if (arrayList.get(i) < arrayList.get(j)) {
                    int temp = arrayList.get(i);
                    arrayList.set(i,arrayList.get(j));
                    arrayList.set(j,temp);
                }
            }
        }
        System.out.println("ArrayList After Sorting");
        System.out.println(arrayList);
    }
}
