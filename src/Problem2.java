import  java.util.Scanner;
public class Problem2 {
    public static void main(String[] args) {
     String str;
     Scanner sc = new Scanner(System.in);
     str = sc.nextLine();
     char[] charArray = str.toCharArray();
     boolean foundSpace = true;
     for(int i = 0; i < str.length(); i++){
         if(Character.isLetter(charArray[i])) {
             if (foundSpace) {
                 charArray[i] = Character.toUpperCase(charArray[i]);
                 foundSpace = false;
             }
         }
         else {
             foundSpace = true;
         }

     }
     String message = String.valueOf(charArray);
     System.out.println(message);
    }
}
