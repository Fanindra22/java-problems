import java.util.*;
public class Problem3 {
    public static void main(String[] args) {
        String str1,str2;
        System.out.println("Enter two String");
        Scanner sc = new Scanner(System.in);
        str1 = sc.nextLine();
        str2 = sc.nextLine();
        if(str1.length() == str2.length()) {
            str1 = str1.toLowerCase();
            str2 = str2.toLowerCase();
            char[] charArray1 = str1.toCharArray();
            char[] charArray2 = str2.toCharArray();

            Arrays.sort(charArray1);
            Arrays.sort(charArray2);

            if (Arrays.equals(charArray1,charArray2)) {
                System.out.println("Both String are Anagram");
            } else {
                System.out.println("There are not Anagram");
            }
        }
        else {
            System.out.println("There are not Anagram");
        }

    }
}
