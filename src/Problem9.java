import java.util.*;
import java.util.ArrayList;

public class Problem9 {
    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }
    public static void main(String[] args) {
        String choice;
        int data;
        ArrayList<Integer> originalArrayList = new ArrayList<Integer>();
        ArrayList<Integer> nonDuplicateArrayList = new ArrayList<Integer>();
        System.out.println("Enter the all the elements of Array List (Enter Alphabet to break)");
        while(true){
            Scanner sc = new Scanner(System.in);
            choice = sc.next();
            if(!isNumeric(choice)){
                break;
            }
            data = Integer.parseInt(choice);
            originalArrayList.add(data);
        }

        Iterator itr = originalArrayList.iterator();
        while(itr.hasNext()){
            int num = (int) itr.next();
            if(!nonDuplicateArrayList.contains(num)){
                nonDuplicateArrayList.add(num);
            }
        }

        Iterator itr1 = nonDuplicateArrayList.iterator();
        while(itr1.hasNext()) {
            System.out.println(itr1.next());
        }
    }
}
