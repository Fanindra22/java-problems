import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class Problem12 {
    public static boolean isNumeric(String str) {
        try {
            Double.parseDouble(str);
            return true;
        } catch(NumberFormatException e){
            return false;
        }
    }
    public static void main(String[] args) {
        String choice;
        int data,i;
        ArrayList<Integer> arrayList = new ArrayList<Integer>();
        System.out.println("Enter the all the elements of Array List (Enter Alphabet to break)");
        while (true) {
            Scanner sc = new Scanner(System.in);
            choice = sc.next();
            if (!isNumeric(choice)) {
                break;
            }
            data = Integer.parseInt(choice);
            arrayList.add(data);
        }
        Collections.sort(arrayList);
        for( i =1; i < arrayList.get(arrayList.size()-1) + 1; i++){
            if(!arrayList.contains(i)){
                break;
            }
        }
        System.out.println(i);
    }
}
