import  java.util.*;
public class Problem5 {
    public static int noOfEligiblePeople=0;
    public static void main(String[] args) {
        int noOfPeople;
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter No of Total People");
        noOfPeople = sc.nextInt();
        int[] ageArray = new int[noOfPeople];
        System.out.println("Enter age of all the People");
        for(int i=0; i < noOfPeople; i++){
            ageArray[i] = sc.nextInt();
        }
        for(int i=0; i < noOfPeople; i++) {
          try{
              ageValidation(ageArray[i]);
          }
          catch(Exception e){
              System.out.println("Exception Occures" + e);
          }
        }
        System.out.println(noOfEligiblePeople);
    }
    static void ageValidation(int age) throws InvalidAgeException{
        if(age<18){
            throw new InvalidAgeException("Invalid Age for Voter Card");
        }
        else{
            System.out.println("Eligible for Vote");
            noOfEligiblePeople++;
        }
    }
}
