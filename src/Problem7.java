import  java.util.*;

public class Problem7 {
    public static void main(String[] args) {
        String str;
        System.out.println("Enter a String");
        Scanner sc = new Scanner(System.in);
        str = sc.nextLine();
        FrequencyCounter fc = new FrequencyCounter();
        fc.count_freq(str);
    }
}
